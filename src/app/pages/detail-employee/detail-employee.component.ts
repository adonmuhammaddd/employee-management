import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../../interfaces/employee';
import { EmployeesComponent } from '../employees/employees.component';
import { GlobalService } from '../../services/global.service';
import { CommonModule, Location } from '@angular/common';
import { RupiahPipe } from '../../pipes/rupiah.pipe';

@Component({
  selector: 'app-detail-employee',
  standalone: true,
  imports: [ EmployeesComponent, CommonModule, RupiahPipe ],
  providers: [ RupiahPipe ],
  templateUrl: './detail-employee.component.html',
  styleUrl: './detail-employee.component.css'
})
export class DetailEmployeeComponent implements OnInit {

  employeeDetailData?: Employee

  constructor(
    private router: Router,
    private globalService: GlobalService,
    private _location: Location
  ) {}

  ngOnInit(): void {
    console.log("this.globalService.employeeDetailData ====> ", this.globalService.employeeDetailData)
    this.employeeDetailData = this.globalService.employeeDetailData
    if (this.globalService.employeeDetailData === undefined) {
      this.router.navigate(['employees'])
    }
  }

  back() {
    this._location.back()
  }
}
