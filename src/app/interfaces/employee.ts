
export interface Employee {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: Date;
  basicSalary: number;
  status: string;
  group: string;
  description: Date;
  [key: string]: any;
}

export interface Group {
  name: string
  status: boolean
}
