import { Routes } from '@angular/router';
import { AddEmployeeComponent } from './pages/add-employee/add-employee.component';
import { unsavedChangesGuard } from './guards/unsaved-changes.guard';
import { EmployeesComponent } from './pages/employees/employees.component';
import { DetailEmployeeComponent } from './pages/detail-employee/detail-employee.component';
import { LayoutComponent } from './components/layout/layout.component';
import { LoginComponent } from './pages/login/login.component';

export const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: 'employees', component: EmployeesComponent},
      { path: 'add-employee', component: AddEmployeeComponent, canDeactivate: [unsavedChangesGuard] },
      { path: 'detail-employee', component: DetailEmployeeComponent }
    ]
  }
];
