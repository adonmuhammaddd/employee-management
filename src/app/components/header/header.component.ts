import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [ RouterLink ],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {

  sidebarTrigger: boolean = false

  constructor(private authService: AuthService){}

  toggleSidebar() {
    this.sidebarTrigger = !this.sidebarTrigger
  }

  logout() {
    this.authService.logout()
  }
}
