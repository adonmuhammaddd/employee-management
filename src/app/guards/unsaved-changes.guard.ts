import { CanDeactivateFn } from '@angular/router';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

export interface CanComponentDeactivate {
  canDeactivate: () => boolean;
}

export const unsavedChangesGuard: CanDeactivateFn<CanComponentDeactivate> = (component: CanComponentDeactivate) => {
  return component.canDeactivate ? component.canDeactivate() : true;
};
