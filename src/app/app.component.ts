import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [ RouterOutlet, RouterModule ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  animations: [
    trigger('toggleSidebar', [
      state('false', style({ opacity: 0, translate: "y-1" })),
      state('true', style({ opacity: 1, translate: "y-0"  })),
      transition('false => true', animate('200ms ease-in')),
      transition('true => false', animate('200ms ease-out'))
    ]),
  ]
})
export class AppComponent {
  title = 'Employee Management';

}
