# Employee Management System

## Demo

- Access On: [https://employee-management-kappa-rouge.vercel.app/](https://employee-management-kappa-rouge.vercel.app/)

  | Username      | Password |
  | ----------    | :------- |
  | adon@mail.com | passw0rd |

## Features

[:arrow_up: Back to top](#index)

| Features                    |
| ----------                  |
| Auth (Login / Logout)       |
| Employee List               |
| Pop up Detail Employee      |
| Add Employee                |
| Validation Unsaved Changes  |

## Installation

#### Pre-Requisites
- Node JS : 20.x.x

[:arrow_up: Back to top](#index)

#### Installing dependencies

- Angular CLI: 17.3.8
- angular-datatables: ^17.1.0
- Typescript: ~5.4.2
- TailwindCSS: ^3.4.3
- PostCSS: ^8.4.38
- Autoprefixer: ^10.4.19
- crypto-js: ^4.2.0
- moment: ^2.30.1
- rxjs: ~7.8.0
- SweetAlert2: ^11.11.0

#### Download and setup

- Clone repo

- Install dependencies
  ```
  $ npm install
  ```
- Start local server
  ```
  $ ng serve
  ```

  ```
  NOTE: Ignore if there is "ERROR ReferenceError: $ is not defined" on terminal
  ```
#### Use the app

[:arrow_up: Back to top](#index)

- Access On Local: [http://localhost:4200](http://localhost:4200)

  | Username      | Password |
  | ----------    | :------- |
  | adon@mail.com | passw0rd |
