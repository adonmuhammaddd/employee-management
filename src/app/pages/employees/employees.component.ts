import { Component, OnInit, OnDestroy, Renderer2, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Router, RouterModule } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { DataTablesModule } from 'angular-datatables';
import { Config } from 'datatables.net-dt';
import { Employee } from '../../interfaces/employee';
import { RupiahPipe } from '../../pipes/rupiah.pipe';

declare var $: any;

@Component({
  selector: 'app-employees',
  standalone: true,
  imports: [ HttpClientModule, RouterModule, FormsModule, DataTablesModule, RupiahPipe ],
  templateUrl: './employees.component.html',
  styleUrl: './employees.component.css'
})
export class EmployeesComponent implements OnInit, AfterViewInit, OnDestroy {

  dtOptions: Config = {};
  dtTrigger: Subject<any> = new Subject();

  data: any[] = [];

  @Output() outputDetailEmployee = new EventEmitter<Employee>()

  constructor(private http: HttpClient, public globalService: GlobalService, private router: Router) {}

  ngOnInit(): void {

    this.dtOptions = {
      destroy: true,
      processing: true,
      pagingType: 'full_numbers',
      pageLength: 10
    };
    setTimeout(() => {
      this.http.get<any[]>('../assets/data.json').subscribe(
        data => {
          this.data = data;
          this.dtTrigger.next(null);
        },
        error => {
          console.error('Error loading data', error);
        }
      );

    }, 1000);

  }

  ngAfterViewInit() {


  }

  // ngOnDestroy(): void {
  //   if (this.dataTable) {
  //     this.dataTable.destroy();
  //   }
  // }

  // applySearch() {
  //   $('#empl-table').DataTable().search(this.globalService.searchText).draw();
  // }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  actionValidate(action: string, data: any) {
    if (action === 'delete') {
      this.globalService.setConfirmationAlert(
        action,
        'You will be delete '+data.firstName+' '+data.lastName+'!',
        '#d33'
      )
    } else {
      console.log('edit')
      this.globalService.setConfirmationAlert(
        action,
        'You will be edit '+data.firstName+' '+data.lastName+'!',
        '#f90'
      )
    }
  }

  detailEmployee(data: Employee) {
    this.globalService.employeeDetailData = data
    this.router.navigate(
      ['detail-employee'],
      { queryParams: { id: data.id } }
    );
  }
}
