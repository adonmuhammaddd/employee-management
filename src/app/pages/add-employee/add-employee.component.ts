import { Component } from '@angular/core';
import { NumberOnlyDirective } from '../../directives/number-only.directive';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Group } from '../../interfaces/employee';
import { CommonModule, Location } from '@angular/common';
import { AbstractControl, FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';

import moment from 'moment';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-add-employee',
  standalone: true,
  imports: [NumberOnlyDirective, HttpClientModule, FormsModule, ReactiveFormsModule, CommonModule],
  providers: [ CurrencyPipe ],
  templateUrl: './add-employee.component.html',
  styleUrl: './add-employee.component.css'
})
export class AddEmployeeComponent {

  maxDate = moment().format('YYYY-MM-DD')
  maxDateDesc = moment().format('YYYY-MM-DD HH:mm:ss')

  title: string = 'Add Employee'

  groups: Group[] = []

  registerForm: FormGroup = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    username: new FormControl(''),
    email: new FormControl(''),
    birthDate: new FormControl(''),
    basicSalary: new FormControl(''),
    status: new FormControl(''),
    group:  new FormControl(''),
    description: new FormControl('')
  })

  status = [
    { name: 'Full-time', value: 'fulltime' },
    { name: 'Contract', value: 'contract' }
  ]

  submitted = false

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private _location: Location,
    private globalService: GlobalService,
    private currencyPipe: CurrencyPipe
  ) {}

  ngOnInit(): void {
    this.http.get('../../../assets/group-constant.json')
      .subscribe({
        next: (data: any) => {
          console.log(data)
          this.groups = data
        }
    })

    this.registerForm = this.formBuilder.group(
      {
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        username: [
          '',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(20)
          ]
        ],
        email: ['', [Validators.required, Validators.email]],
        birthDate: ['', Validators.required],
        basicSalary: ['', Validators.required],
        status: [this.status[0], Validators.required],
        group: [this.groups[0], Validators.required],
        description: ['', Validators.required]
      }
    )

    this.registerForm.valueChanges.subscribe(form => {
      if (form.basicSalary) {
        this.registerForm.patchValue({
          basicSalary: this.currencyPipe.transform(form.basicSalary.replace(/\D/g, '').replace(/^0+/, ''), 'Rp ', 'symbol', '1.0-0')
        }, {emitEvent: false})
      }
    })
  }


  get f(): { [key: string]: AbstractControl } {
    return this.registerForm.controls
  }

  onSubmit(): void {
    this.submitted = true

    if (this.registerForm.invalid) {
      return
    } else {
      this.globalService.setConfirmationAlert(
        'submit',
        'You will be save this record!',
        '#006'
      )
    }
  }

  onReset(): void {
    this.submitted = false;
    this.registerForm.reset();
  }

  back() {
    this._location.back()
  }

  canDeactivate(): boolean {
    if (this.registerForm.dirty) {
      return confirm('You have unsaved changes. Do you really want to leave?');
    }
    return true;
  }
}
